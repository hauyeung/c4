#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
/* put any other #includes you need here */
/* This is the function prototype -- more about that topic coming in chapter 9 */
int get_int(void);

int main(void)
{
/* Declare any variables you need */
/* Display a message to the user that to terminate input s/he should enter the value 0. */
/* Write a loop that uses get_int() to get an integer and stops when the integer it gets is 0. */
	get_int();
	return 0;
}

/* This is the function definition -- more about that topic coming in chapter 9 */
int get_int(void)
{
	int num = -1, val = 0;
	char s[256];
	printf("Enter some integers. When you are done enter 0 (zero).\n ");
	fgets(s, 256, stdin);
	char * pch = strtok(s, " ");	
	while (pch != NULL)
	{
		char * end;
		strtod(pch, &end);
		if (atoi(pch) == 0)
		{
			if (isdigit(*pch))
			{
				break;
			}
			else			
			{
				printf("%s is not an integer\n", pch);			
			}
		}
		else
		{
			int isnum = 1;
			for (int i = 0; i < strlen(pch) ; i++)
			{
				if (isalpha(pch[i]))
				{
					isnum = 0;
				}
			}
			if (isnum ==1)
			{	
				printf("I got the number %s\n", pch);
			}
			else
			{
				printf("%s is not an integer\n", pch);		
			}
		}
		pch = strtok(NULL, " ");
	}
	return 0;
}