/* guess.c -- an inefficient and faulty number-guesser */
#include <stdio.h>
int main(void)
{
	int guess = 50, min = 1, max = 101;
	char s = 'l';
	printf("Is it too high (h), too low (l) or correct (c)\n");
	while (s != 'c')
	{		
		switch (s)
		{
			case 'l':
				guess = ((guess + max) /2);
				break;
			case 'h':
				guess = ((min + guess)/2);
				break;
			case 'c':
				break;
		}
		printf("Is it %d\n", guess);
		scanf("%c", &s);
	}
	printf ("You guessed right.");
	return 0;
}